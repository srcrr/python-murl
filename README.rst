========
Overview
========

.. start-badges

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - | |travis| |appveyor| |requires|
        | |codecov|
    * - package
      - | |version| |wheel| |supported-versions| |supported-implementations|
        | |commits-since|
.. |docs| image:: https://readthedocs.org/projects/python-murl/badge/?style=flat
    :target: https://python-murl.readthedocs.io/
    :alt: Documentation Status

.. |travis| image:: https://api.travis-ci.com/srcrr/python-murl.svg?branch=master
    :alt: Travis-CI Build Status
    :target: https://travis-ci.com/github/srcrr/python-murl

.. |appveyor| image:: https://ci.appveyor.com/api/projects/status/github/srcrr/python-murl?branch=master&svg=true
    :alt: AppVeyor Build Status
    :target: https://ci.appveyor.com/project/srcrr/python-murl

.. |requires| image:: https://requires.io/github/srcrr/python-murl/requirements.svg?branch=master
    :alt: Requirements Status
    :target: https://requires.io/github/srcrr/python-murl/requirements/?branch=master

.. |codecov| image:: https://codecov.io/gh/srcrr/python-murl/branch/master/graphs/badge.svg?branch=master
    :alt: Coverage Status
    :target: https://codecov.io/github/srcrr/python-murl

.. |version| image:: https://img.shields.io/pypi/v/murl.svg
    :alt: PyPI Package latest release
    :target: https://pypi.org/project/murl

.. |wheel| image:: https://img.shields.io/pypi/wheel/murl.svg
    :alt: PyPI Wheel
    :target: https://pypi.org/project/murl

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/murl.svg
    :alt: Supported versions
    :target: https://pypi.org/project/murl

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/murl.svg
    :alt: Supported implementations
    :target: https://pypi.org/project/murl

.. |commits-since| image:: https://img.shields.io/github/commits-since/srcrr/python-murl/v0.0.0.svg
    :alt: Commits since latest release
    :target: https://github.com/srcrr/python-murl/compare/v0.0.0...master



.. end-badges

An example package. Generated with cookiecutter-pylibrary.

* Free software: BSD 2-Clause License

Installation
============

::

    pip install murl

You can also install the in-development version with::

    pip install https://github.com/srcrr/python-murl/archive/master.zip


Documentation
=============


https://python-murl.readthedocs.io/


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
